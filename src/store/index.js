import Vue from 'vue';
import Vuex from 'vuex';
// import quand nous avons plusieurs export cont
// dans les import il faut alors se mettre
import { state } from './state';
import getters from './getters';
import mutations from './mutations';
// import plugins from './plugins';

Vue.use(Vuex);

export default new Vuex.Store({
  state,
  getters,
  mutations,
  // plugins,
});
